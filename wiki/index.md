# Table of contents

* [Nature of the India Bourgeoisie (1967)](public/1-nature-of-the-india-bourgeoisie-1967.md)
* [Global capitalist-imperialism](public/2-global-capitalist-imperialism.md)
* [Interview with the General Secretary of the Communist Party of India (Maoist)](public/3-interview-with-the-general-secretary-of-the-communist-party-of-india-maoist.md)
* [Process of Evolution of Indian Society (2020)](public/4-process-of-evolution-of-indian-society-2020.md)
